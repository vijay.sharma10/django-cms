from django.apps import AppConfig


class DynasiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dynasite'
